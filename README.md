# Parser for log file #

Java parser for log file. Application loads log into MySQL table and finds IPs that exceed limit during specified interval.
Found ips are copied into separate table with description.

### How to use ###
1. To build jar you need to run 'mvn package' in the root of the project
2. Change to directory target
3. Run java -jar wallethub-1.0.jar --accesslog=path/to/file --startDate=yyyy-MM-dd.HH:mm:ss --duration=hourly --threshold=100
Application loads parser.properties file, which resides in the same directory as jar. File 'parser.properties' contains
credentials and url to connect to MySQL.

### Command line arguments ###
**--accesslog**

Optional argument. If present, then it points to log file, which should be uploaded.
If argument is absent, application assumes that log file is already uploaded to MySQL.

**--startDate**

Obligatory argument. Should contains 'start point' of interval, during which application will search for ips with exceeded
limits. Date from argument should be in format yyyy-MM-dd.HH:mm:ss

**--duration**

Obligatory argument. Could have value 'hourly' or 'daily'. This argument is used to calculate 'end point' of the interval
during which application will search for ips with exceeded limits.

**--threshold**

Obligatory argument. It represents number (or "limit"), which is used to detect maximum requests from ip during specified period.
It is expected that threshold could not be greater than 500 for 'daily' duration or not greater than 200 for 'hourly' duration.
