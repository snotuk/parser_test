import config.CommandLineArguments;
import config.DatabaseConfig;
import dao.BlockedIp;
import dao.LogRecord;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;

public class ApplicationTest {
    private static final DatabaseConfig dbConfig = StartParser.loadDatabaseConfig("src/test/java/resources/parser.properties");

    @Test
    public void testThatDatabaseConfigIsBuiltFromFile() throws IOException {
        assertThat(dbConfig.getDbUrl(), startsWith("jdbc:mysql://localhost:3306/wallet"));
        assertThat(dbConfig.getPass(), equalTo("parser12"));
        assertThat(dbConfig.getUser(), equalTo("parser"));
    }

    @Test
    public void testThatBlocksCorrectIp() throws IOException {
        DbRequestsForTesting requests = new DbRequestsForTesting(dbConfig);
        requests.initializeLogTable();
        String[] args = new String[]{"--accesslog=src/test/java/resources/111+222.txt", "--startDate=2017-01-11.12:00:00", "--duration=hourly", "--threshold=10"};
        StartParser startParser = new StartParser(new CommandLineArguments(args), dbConfig);
        startParser.parse();

        //test that ips with ending 111 and 222 are in block list
        List<BlockedIp> blockedIps = requests.getAllBlockedIps();
        assertThat(blockedIps.size(), equalTo(2));

        String expectedIp = "192.168.31.111";
        BlockedIp actual = blockedIps.stream().filter(blocked -> blocked.getIp().equals(expectedIp)).findFirst().orElseThrow(() -> new RuntimeException("Not found " + expectedIp));
        assertBlockingIpsAreEqual(actual, expectedBlockedIp(expectedIp));

        String anotherExpectedIp = "192.168.31.222";
        actual = blockedIps.stream().filter(blocked -> blocked.getIp().equals(anotherExpectedIp)).findFirst().orElseThrow(() -> new RuntimeException("Not found " + anotherExpectedIp));
        assertBlockingIpsAreEqual(actual, expectedBlockedIp(anotherExpectedIp));
    }

    @Test
    public void testThatLogImportingIsSkippedIfNoLogFileIsProvided() {
        DbRequestsForTesting testRequests = new DbRequestsForTesting(dbConfig);
        long logRecordsCount = testRequests.count(LogRecord.TABLE_NAME);
        long blockedIpsCount = testRequests.count(BlockedIp.TABLE_NAME);

        String[] args = new String[]{"--startDate=2017-01-20.12:00:00", "--duration=hourly", "--threshold=100"};
        StartParser startParser = new StartParser(new CommandLineArguments(args), dbConfig);
        startParser.parse();
        assertThat(logRecordsCount, equalTo(testRequests.count(LogRecord.TABLE_NAME)));
        assertThat(blockedIpsCount, equalTo(testRequests.count(BlockedIp.TABLE_NAME)));
    }

    @Test
    public void testSearchLogsByIp() {
        DbRequestsForTesting requests = new DbRequestsForTesting(dbConfig);
        requests.initializeLogTable();
        String[] args = new String[]{"--accesslog=src/test/java/resources/111+222.txt", "--startDate=2017-01-11.12:00:00", "--duration=hourly", "--threshold=10"};
        StartParser startParser = new StartParser(new CommandLineArguments(args), dbConfig);
        startParser.parse();

        List<LogRecord> records = requests.findRequestsByIp("192.168.31.111");
        assertThat(records.size(), equalTo(10));
    }

    private BlockedIp expectedBlockedIp(String ip) {
        return new BlockedIp().setIp(ip).setRequestsLimit(10).setRequestsActual(10).setReason("Exceeded limit in HOURLY period starting from 2017-01-11T12:00 to 2017-01-11T13:00");
    }

    private void assertBlockingIpsAreEqual(BlockedIp actual, BlockedIp expected) {
        assertThat(actual.getIp(), equalTo(expected.getIp()));
        assertThat(actual.getRequestsLimit(), equalTo(expected.getRequestsLimit()));
        assertThat(actual.getRequestsActual(), equalTo(expected.getRequestsActual()));
        assertThat(actual.getReason(), equalTo(expected.getReason()));
    }
}
