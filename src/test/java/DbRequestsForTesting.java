import config.DatabaseConfig;
import dao.BlockedIp;
import dao.LogRecord;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

class DbRequestsForTesting {
    private final DatabaseConfig config;

    DbRequestsForTesting(DatabaseConfig config) {
        this.config = config;
    }

    List<BlockedIp> getAllBlockedIps() {
        List<BlockedIp> allBlockedIps = new ArrayList<>();
        String query = String.format("select INET_NTOA(%s), %s, %s, %s from %s;",
                BlockedIp.IP_COLUMN, BlockedIp.REQUESTS_LIMIT_COLUMN, BlockedIp.REQUESTS_ACTUAL_COLUMN, BlockedIp.REASON_COLUMN, BlockedIp.TABLE_NAME);
        try (Connection connection = DriverManager.getConnection(config.getDbUrl(), config.getUser(), config.getPass());
             PreparedStatement statement = connection.prepareStatement(query)) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                allBlockedIps.add(new BlockedIp().setIp(rs.getString(1)).setRequestsLimit(rs.getInt(2))
                        .setRequestsActual(rs.getInt(3)).setReason(rs.getString(4)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to initialize table in DB.");
        }
        return allBlockedIps;
    }

    long count(String tableName) {
        String query = "select count(*) from " + tableName;
        long count = 0;
        try (Connection connection = DriverManager.getConnection(config.getDbUrl(), config.getUser(), config.getPass());
             PreparedStatement statement = connection.prepareStatement(query)) {
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                count = rs.getLong(1);
            }
            return count;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to initialize table in DB.");
        }
    }

    List<LogRecord> findRequestsByIp(String ip) {
        String query = String.format("select %s, INET_NTOA(%s), %s, %s, %s from %s where INET_NTOA(%s)='%s'",
                LogRecord.DATETIME_COLUMN, LogRecord.IP_COLUMN, LogRecord.REQUEST_COLUMN, LogRecord.STATUS_COLUMN, LogRecord.USER_AGENT_COLUMN,
                LogRecord.TABLE_NAME, LogRecord.IP_COLUMN, ip);
        List<LogRecord> records = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(config.getDbUrl(), config.getUser(), config.getPass());
             PreparedStatement statement = connection.prepareStatement(query)) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                records.add(new LogRecord(rs.getTimestamp(1).toLocalDateTime(), rs.getString(2), rs.getString(3)
                        , rs.getInt(4), rs.getString(5)));
            }
            return records;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to initialize table in DB.");
        }
    }

    public void initializeLogTable() {
        String dropTableIfExists = "drop table if exists ";
        String createNewTableForLogs = String.format("create table %s (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, %s TIMESTAMP(3), %s INT UNSIGNED, %s TEXT, %s INT, %s TEXT);",
                LogRecord.TABLE_NAME, LogRecord.DATETIME_COLUMN, LogRecord.IP_COLUMN, LogRecord.REQUEST_COLUMN, LogRecord.STATUS_COLUMN, LogRecord.USER_AGENT_COLUMN);
        String createNewTableForBlockedIps = String.format("create table %s (%s INT UNSIGNED PRIMARY KEY, %s INT, %s INT, %s TEXT);",
                BlockedIp.TABLE_NAME, BlockedIp.IP_COLUMN, BlockedIp.REQUESTS_LIMIT_COLUMN, BlockedIp.REQUESTS_ACTUAL_COLUMN, BlockedIp.REASON_COLUMN);

        try (Connection connection = DriverManager.getConnection(config.getDbUrl(), config.getUser(), config.getPass());
             PreparedStatement statement = connection.prepareStatement(dropTableIfExists + LogRecord.TABLE_NAME)) {
            statement.executeUpdate();
            statement.executeUpdate(dropTableIfExists + BlockedIp.TABLE_NAME);
            statement.executeUpdate(createNewTableForLogs);
            statement.executeUpdate(createNewTableForBlockedIps);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to initialize table in DB.");
        }
    }
}
