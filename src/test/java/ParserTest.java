import dao.LogRecord;
import org.junit.Test;
import parser.LogParser;

import java.time.LocalDateTime;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ParserTest {

    private static LogParser parser = new LogParser();

    @Test(expected = IllegalArgumentException.class)
    public void testThatParserFailesIfLogHasUnexpectedFormat() {
        String incorrectLogLine = "2017-01-01 00:01:06.110;192.168.159.178;\"GET / HTTP/1.1\"|200|\"Dalvik/2.1.0 (Linux; U; Android 6.0; vivo 1601 Build/MRA58K)";
        parser.parseLine(incorrectLogLine);
    }

    @Test
    public void testThatParserParsesLogLineCorrectly() {
        String source = "2017-01-01 00:00:21.164|192.168.234.82|\"GET / HTTP/1.1\"|200|\"swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0\"";
        LogRecord record = parser.parseLine(source);
        assertThat(record.getDateTime(), equalTo(LocalDateTime.of(2017, 1, 1, 0, 0, 21, 164000000)));
        assertThat(record.getIp(), equalTo("192.168.234.82"));
        assertThat(record.getRequest(), equalTo("GET / HTTP/1.1"));
        assertThat(record.getStatus(), equalTo(200));
        assertThat(record.getUserAgent(), equalTo("swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0"));
    }


}
