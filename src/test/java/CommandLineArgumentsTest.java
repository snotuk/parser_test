import config.CommandLineArguments;
import config.Duration;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

public class CommandLineArgumentsTest {

    private final String accessLog = "pathToFile";
    private final String startDate = "2017-01-01.13:00:00";
    private final String duration = "hourly";
    private final String threshold = "100";

    @Test(expected = IllegalArgumentException.class)
    public void testThatExceptionIsThrownIfObligatoryCommandLineArgumentIsMissing() {
        String[] args = new String[]{"--accesslog=" + accessLog, "--startDate=" + startDate, "--duration=" + duration};
        new CommandLineArguments(args);
    }

    @Test
    public void testThatCommandLineArgumentsAreCorrectlyInitialized() {
        String[] correctArgs = commandLineArguments(accessLog, startDate, duration, threshold);
        CommandLineArguments arguments = new CommandLineArguments(correctArgs);
        assertThat(arguments.getFilename(), equalTo(accessLog));
        assertThat(arguments.getThreshold() + "", equalTo(threshold));
        assertThat(arguments.getDateTime(), equalTo(LocalDateTime.of(2017, 1, 1, 13, 0, 0)));
        assertThat(arguments.getDuration(), equalTo(Duration.fromCommandLineValue(duration)));
    }

    @Test
    public void testThatExceptionIsThrownIfUnknownArgumentIsPresent() {
        String[] args = correctCommandLineArguments();
        assertTrue(args.length > 2);
        String incorrectArgument = "--unknownArg";
        args[0] = incorrectArgument + "=" + threshold;
        try {
            new CommandLineArguments(args);
            fail("Exception must be thrown if command line arguments has unknown argument");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage(), containsString(incorrectArgument));
        }
    }

    @Test
    public void testThatDurationArgumentCouldHaveOnlyCertainValues() {
        String[] args = commandLineArguments(accessLog, startDate, "yearly", threshold);
        try {
            new CommandLineArguments(args);
            fail("Exception must be thrown because duration has incorrect value");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage(), containsString("yearly"));
        }
        new CommandLineArguments(commandLineArguments(accessLog, startDate, "hourly", threshold));
        new CommandLineArguments(commandLineArguments(accessLog, startDate, "daily", threshold));
    }

    @Test
    public void testThatThresholdArgumentShouldNotExceedDurationLimits() {
        String[] args = commandLineArguments(accessLog, startDate, duration, "500");
        try {
            new CommandLineArguments(args);
            fail("Exception must be thrown because threshold exceeds maximum value");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage(), containsString("Threshold should be less than or equals"));
        }
    }

    @Test(expected = DateTimeParseException.class)
    public void testThatValueForStartDateArgumentMustHaveCorrectFormat() {
        String[] args = commandLineArguments(accessLog, LocalDateTime.now().toString(), "yearly", threshold);
        new CommandLineArguments(args);
    }

    @Test
    public void testExceptionIsThrownIfArgumentHasNoValue() {
        String[] noValueArgument = correctCommandLineArguments();
        noValueArgument[0] = "--accesslog";
        try {
            new CommandLineArguments(noValueArgument);
            fail("Exception must be thrown because pathToFileIsEmpty");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage(), containsString("<argumentName>=<argumentValue>"));
        }
    }

    private String[] correctCommandLineArguments() {
        return commandLineArguments(accessLog, startDate, duration, threshold);
    }

    private String[] commandLineArguments(String pathToFile, String dateTime, String duration, String threshold) {
        return new String[]{"--accesslog=" + pathToFile, "--startDate=" + dateTime, "--duration=" + duration, "--threshold=" + threshold};
    }
}
