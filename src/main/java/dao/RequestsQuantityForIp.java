package dao;

public class RequestsQuantityForIp {
    private final String ip;
    private final int requestsQuantity;

    RequestsQuantityForIp(String ip, int requestsQuantity) {
        this.ip = ip;
        this.requestsQuantity = requestsQuantity;
    }

    public String getIp() {
        return ip;
    }

    public int getRequestsQuantity() {
        return requestsQuantity;
    }
}
