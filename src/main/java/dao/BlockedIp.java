package dao;

public class BlockedIp {
    public static final String TABLE_NAME = "blocked_ip";
    public static final String IP_COLUMN = "ip";
    public static final String REASON_COLUMN = "reason";
    public static final String REQUESTS_LIMIT_COLUMN = "requests_limit";
    public static final String REQUESTS_ACTUAL_COLUMN = "requests_actual";

    private String ip;
    private String reason;
    private int requestsLimit;
    private int requestsActual;

    public String getIp() {
        return ip;
    }

    public BlockedIp setIp(String ip) {
        this.ip = ip;
        return this;
    }

    public String getReason() {
        return reason;
    }

    public BlockedIp setReason(String reason) {
        this.reason = reason;
        return this;
    }

    public int getRequestsLimit() {
        return requestsLimit;
    }

    public BlockedIp setRequestsLimit(int requestsLimit) {
        this.requestsLimit = requestsLimit;
        return this;
    }

    public int getRequestsActual() {
        return requestsActual;
    }

    public BlockedIp setRequestsActual(int requestsActual) {
        this.requestsActual = requestsActual;
        return this;
    }
}
