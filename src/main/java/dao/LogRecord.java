package dao;

import java.time.LocalDateTime;

public class LogRecord {
    public static final String TABLE_NAME = "log_record";
    public static final String DATETIME_COLUMN = "date_time";
    public static final String IP_COLUMN = "ip";
    public static final String REQUEST_COLUMN = "request";
    public static final String STATUS_COLUMN = "status";
    public static final String USER_AGENT_COLUMN = "user_agent";

    private final LocalDateTime dateTime;
    private final String ip;
    private final String request;
    private final int status;
    private final String userAgent;

    public LogRecord(LocalDateTime dateTime, String ip, String request, int status, String userAgent) {
        this.dateTime = dateTime;
        this.ip = ip;
        this.request = request;
        this.status = status;
        this.userAgent = userAgent;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public String getIp() {
        return ip;
    }

    public String getRequest() {
        return request;
    }

    public int getStatus() {
        return status;
    }

    public String getUserAgent() {
        return userAgent;
    }
}
