package dao;

import config.DatabaseConfig;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class DatabaseRequests {
    private final DatabaseConfig config;

    public DatabaseRequests(DatabaseConfig config) {
        this.config = config;
    }

    public void batchInsertLogRecords(List<LogRecord> records) {
        if (records.isEmpty()) {
            return;
        }
        String insertSQL = String.format("insert into %s (%s, %s, %s, %s, %s) values (?, INET_ATON(?), ?, ?, ?)",
                LogRecord.TABLE_NAME, LogRecord.DATETIME_COLUMN, LogRecord.IP_COLUMN, LogRecord.REQUEST_COLUMN, LogRecord.STATUS_COLUMN, LogRecord.USER_AGENT_COLUMN);

        Consumer<PreparedStatement> batchProcessing = statement -> {
            for (LogRecord record : records) {
                try {
                    statement.setTimestamp(1, Timestamp.valueOf(record.getDateTime()));
                    statement.setString(2, record.getIp());
                    statement.setString(3, record.getRequest());
                    statement.setInt(4, record.getStatus());
                    statement.setString(5, record.getUserAgent());
                    statement.addBatch();
                } catch (SQLException ex) {
                    throw new RuntimeException(ex);
                }
            }
        };
        executeBatch(insertSQL, batchProcessing);
    }

    public List<RequestsQuantityForIp> findIps(LocalDateTime startPeriod, LocalDateTime endPeriod, int threshold) {
        String searchQuery = String.format("select INET_NTOA(%s) as users, count(*) as ticks from %s where %s >= ? and %s < ? group by users having ticks>=?;",
                LogRecord.IP_COLUMN, LogRecord.TABLE_NAME, LogRecord.DATETIME_COLUMN, LogRecord.DATETIME_COLUMN);
        List<RequestsQuantityForIp> foundIp = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(config.getDbUrl(), config.getUser(), config.getPass());
             PreparedStatement statement = connection.prepareStatement(searchQuery)) {
            statement.setTimestamp(1, Timestamp.valueOf(startPeriod));
            statement.setTimestamp(2, Timestamp.valueOf(endPeriod));
            statement.setInt(3, threshold);


            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                foundIp.add(new RequestsQuantityForIp(rs.getString(1), rs.getInt(2)));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return foundIp;
    }

    private void executeBatch(String sqlQuery, Consumer<PreparedStatement> statementConsumer) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DriverManager.getConnection(config.getDbUrl(), config.getUser(), config.getPass());
            statement = connection.prepareStatement(sqlQuery);
            connection.setAutoCommit(false);
            statementConsumer.accept(statement);
            statement.executeBatch();
            connection.commit();
        } catch (Exception e) {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                throw new RuntimeException("Exception is thrown on closing statement and connection", e);
            }
        }
    }

    public void batchInsertBlockedIp(List<BlockedIp> blockedIps) {
        if (blockedIps.isEmpty()) {
            return;
        }
        String insertSQL = String.format("insert ignore into %s (%s, %s, %s, %s) values (INET_ATON(?), ?, ?, ?)",
                BlockedIp.TABLE_NAME, BlockedIp.IP_COLUMN, BlockedIp.REQUESTS_LIMIT_COLUMN, BlockedIp.REQUESTS_ACTUAL_COLUMN, BlockedIp.REASON_COLUMN);

        Consumer<PreparedStatement> processingBatch = statement -> {
            for (BlockedIp record : blockedIps) {
                try {
                    statement.setString(1, record.getIp());
                    statement.setInt(2, record.getRequestsLimit());
                    statement.setInt(3, record.getRequestsActual());
                    statement.setString(4, record.getReason());
                    statement.addBatch();
                } catch (SQLException ex) {
                    throw new RuntimeException(ex);
                }
            }
        };
        executeBatch(insertSQL, processingBatch);
    }
}
