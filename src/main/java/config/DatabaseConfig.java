package config;

import java.time.ZoneId;

public class DatabaseConfig {
    private final String dbUrl;
    private final String pass;
    private final String user;
//jdbc:mysql://localhost:3306/wallet?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false

    public DatabaseConfig(String dbUrl, String pass, String user) {
        this.dbUrl = dbUrl + "&serverTimezone=" + ZoneId.systemDefault();
        this.pass = pass;
        this.user = user;
    }

    public String getDbUrl() {
        return dbUrl;
    }

    public String getPass() {
        return pass;
    }

    public String getUser() {
        return user;
    }
}
