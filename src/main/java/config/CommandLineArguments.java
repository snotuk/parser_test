package config;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CommandLineArguments {
    public static Set<String> obligatoryParams = createExpectedStartingParams();
    public static Set<String> optionalParams = Stream.of("--accesslog").collect(Collectors.toSet());
    ;
    private String filename;
    private LocalDateTime dateTime;
    private Duration duration;
    private int threshold;
    private DateTimeFormatter dateFormatForCommandLine = DateTimeFormatter.ofPattern("yyyy-MM-dd.HH:mm:ss", Locale.US);


    public CommandLineArguments(String[] args) {
        initialize(args);
        validateInitializedProperties();
    }

    private static Set<String> createExpectedStartingParams() {
        return Stream.of("--startDate", "--duration", "--threshold").collect(Collectors.toSet());
    }

    private void initialize(String[] args) {
        if (args.length == 0 || args.length < obligatoryParams.size() ||
                !obligatoryParams.stream().allMatch(param -> Arrays.stream(args).anyMatch(arg -> arg.startsWith(param)))) {
            String message = obligatoryParams.stream().collect(Collectors.joining(" "));
            throw new IllegalArgumentException("Following arguments must be present " + message);
        }
        Arrays.stream(args).map(this::parseParam).forEach(this::configureProperty);
    }

    private String[] parseParam(String arg) {
        String[] param = arg.split("=");
        if (param.length != 2) {
            throw new IllegalArgumentException("Please, check that command line argument matches pattern <argumentName>=<argumentValue>: " + arg);
        }
        if (!obligatoryParams.contains(param[0]) && !optionalParams.contains(param[0])) {
            throw new IllegalArgumentException("Unknown argument is used " + param[0]);
        }
        return param;
    }

    private void configureProperty(String[] param) {
        switch (param[0]) {
            case "--accesslog":
                this.filename = param[1];
                break;
            case "--startDate":
                this.dateTime = LocalDateTime.parse(param[1], dateFormatForCommandLine);
                break;
            case "--duration":
                this.duration = Duration.fromCommandLineValue(param[1]);
                break;
            case "--threshold":
                this.threshold = Integer.parseInt(param[1]);
                break;
        }
    }

    private void validateInitializedProperties() {
        if (threshold > duration.getMaxRequests()) {
            throw new IllegalArgumentException("Threshold should be less than or equals to " + duration.getMaxRequests());
        }
    }

    public String getFilename() {
        return filename;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Duration getDuration() {
        return duration;
    }

    public int getThreshold() {
        return threshold;
    }
}
