package config;

import java.util.Arrays;
import java.util.stream.Collectors;

public enum Duration {
    HOURLY("hourly", 200), DAILY("daily", 500);

    private String commandLineValue;
    private int maxRequests;

    Duration(String commandLineValue, int maxRequests) {
        this.commandLineValue = commandLineValue;
        this.maxRequests = maxRequests;
    }

    public int getMaxRequests() {
        return maxRequests;
    }

    public static Duration fromCommandLineValue(String arg) {
        return Arrays.stream(values()).filter(d -> arg.equals(d.commandLineValue)).findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Incorrect value " + arg + ". Correct values for duration are: "
                        + availableValues()));
    }

    private static String availableValues() {
        return Arrays.stream(values()).map(duration -> duration.commandLineValue).collect(Collectors.joining(", "));
    }
}
