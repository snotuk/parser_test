import config.CommandLineArguments;
import config.DatabaseConfig;
import dao.DatabaseRequests;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class StartParser {
    private final CommandLineArguments commandLineArguments;
    private final DatabaseConfig config;

    public StartParser(CommandLineArguments commandLineArguments, DatabaseConfig config) {
        this.commandLineArguments = commandLineArguments;
        this.config = config;
    }

    public static void main(String[] args) throws IOException {
        CommandLineArguments commandLineArguments = new CommandLineArguments(args);
        DatabaseConfig config = loadDatabaseConfig("parser.properties");
        new StartParser(commandLineArguments, config).parse();
    }

    public void parse() {
        DatabaseRequests dbRequests = new DatabaseRequests(config);
        if (commandLineArguments.getFilename() != null) {
            System.out.println("Loading log file into database...");
            LogsLoader logsLoader = new LogsLoader(dbRequests);
            logsLoader.loadLogsToDb(commandLineArguments.getFilename());
        }

        SearchByArguments search = new SearchByArguments(dbRequests);
        search.searchIps(commandLineArguments);
    }

    public static DatabaseConfig loadDatabaseConfig(String fileName) {
        Properties prop = new Properties();
        DatabaseConfig config;
        try (InputStream input = new FileInputStream(fileName)) {
            prop.load(input);
            config = new DatabaseConfig(prop.getProperty("dbUrl"), prop.getProperty("pass"), prop.getProperty("user"));
        } catch (IOException ex) {
            throw new IllegalStateException("File " + fileName + " is not found.");
        }
        return config;
    }
}

