import config.CommandLineArguments;
import dao.BlockedIp;
import dao.DatabaseRequests;
import dao.RequestsQuantityForIp;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class SearchByArguments {
    private final DatabaseRequests dbRequests;

    public SearchByArguments(DatabaseRequests dbRequests) {
        this.dbRequests = dbRequests;
    }

    public void searchIps(CommandLineArguments args) {
        LocalDateTime startPeriod = args.getDateTime();
        List<BlockedIp> ipsToBlock = dbRequests.findIps(startPeriod, getEndPeriod(args), args.getThreshold()).stream()
                .map(requests -> toBlocked(requests, args))
                .collect(Collectors.toList());
        dbRequests.batchInsertBlockedIp(ipsToBlock);
        ipsToBlock.forEach(ip -> System.out.println(ip.getIp()));
    }

    private LocalDateTime getEndPeriod(CommandLineArguments args) {
        switch (args.getDuration()) {
            case HOURLY:
                return args.getDateTime().plusHours(1);
            case DAILY:
                return args.getDateTime().plusDays(1);
            default:
                throw new IllegalArgumentException(args.getDuration() + " has no implementation on endDate calculation");
        }
    }

    private BlockedIp toBlocked(RequestsQuantityForIp requests, CommandLineArguments args) {
        return new BlockedIp().setIp(requests.getIp()).setRequestsLimit(args.getThreshold())
                .setRequestsActual(requests.getRequestsQuantity()).setReason(createReason(args));
    }

    private String createReason(CommandLineArguments args) {
        return "Exceeded limit in " + args.getDuration().toString() + " period starting from " + args.getDateTime() + " to " + getEndPeriod(args);
    }
}
