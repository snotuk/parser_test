package parser;

import dao.LogRecord;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class LogParser {
    private DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);

    public LogRecord parseLine(String source) {
        source = source.replaceAll("\"", "");
        String[] splittedLine = source.split("\\|");
        if (splittedLine.length != 5) {
            throw new IllegalArgumentException("Source log line does not fits pattern <dateTime>|ip|request|status|userAgent");
        }
        LocalDateTime dateTime = LocalDateTime.parse(splittedLine[0], dateFormat);
        int status = Integer.parseInt(splittedLine[3]);
        return new LogRecord(dateTime, splittedLine[1], splittedLine[2], status, splittedLine[4]);
    }
}
