import dao.DatabaseRequests;
import parser.LogParser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LogsLoader {
    private final DatabaseRequests dbRequests;
    private final int BATCH_SIZE = 1000;

    public LogsLoader(DatabaseRequests dbRequests) {
        this.dbRequests = dbRequests;
    }

    public void loadLogsToDb(String logFileName) {
        List<String> linesRead = new ArrayList<>();
        int counter = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(logFileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                counter++;
                linesRead.add(line);
                if (counter >= BATCH_SIZE) {
                    insertLinesAsLogRecords(linesRead);
                    counter = 0;
                    linesRead = new ArrayList<>();
                }
            }
            insertLinesAsLogRecords(linesRead);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Could not process logFile: " + logFileName, e);
        }
    }

    private void insertLinesAsLogRecords(List<String> logLines) {
        LogParser parser = new LogParser();
        dbRequests.batchInsertLogRecords(logLines.stream().map(lineFromLog -> parser.parseLine(lineFromLog)).collect(Collectors.toList()));
    }
}
